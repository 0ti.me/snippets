const get = (obj, keys, def) => {
  const keysRay = Array.isArray(keys) ? keys : keys.split('.');
  const next = obj[keysRay[0]];

  //console.error(5, obj, keysRay, def, next);

  return keysRay.length <= 1 || !next
    ? next !== undefined
      ? next
      : def
    : get(next, keysRay.slice(1), def);
};

module.exports = get;
