const get = require('./get');

const flow = (...args) => item => args.reduce((acc, ea) => ea(acc), item);
const promiseFlow = (...args) => item =>
  args.reduce((acc, ea) => acc.then(ea), Promise.resolve(item));

const fpGet = (key, def) => obj => get(obj, key, def);
const map = cb => list => list.map(cb);

module.exports = {
  flow,
  get: fpGet,
  map,
  promiseFlow,
};
