#!/usr/bin/env node

const statsJson = require('../docker/stats');
const util = require('util');

const inspect = (x, options) =>
  util.inspect(
    x,
    Object.assign(
      {
        colors: true,
        depth: 10,
      },
      options,
    ),
  );

statsJson()
  .then(result => console.log(JSON.stringify(result)))
  .catch(err => {
    console.error(err);

    return -1;
  })
  .then(process.exit);
