#!/usr/bin/env node

const containersJson = require('../docker/containers');

containersJson()
  .then(result => console.log(JSON.stringify(result)))
  .catch(err => {
    console.error(err);

    return -1;
  })
  .then(process.exit);
