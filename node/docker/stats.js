#!/usr/bin/env node

const fp = require('../fp');
const requestPromise = require('../request-promise');

const socketPath = '/var/run/docker.sock';

module.exports = () =>
  Promise.resolve({
    method: 'GET',
    path: '/v1.30/containers/json',
    socketPath,
  })
    .then(requestPromise)
    .then(JSON.parse)
    .then(
      fp.flow(
        fp.map(fp.get('Id')),
        fp.map(id => ({
          method: 'GET',
          path: `/v1.30/containers/${id}/stats?stream=false`,
          socketPath,
        })),
        fp.map(fp.promiseFlow(requestPromise, JSON.parse)),
      ),
    )
    .then(promises => Promise.all(promises));
