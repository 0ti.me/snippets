#!/usr/bin/env node

const requestPromise = require('../request-promise');

module.exports = () =>
  Promise.resolve({
    method: 'GET',
    path: '/v1.30/containers/json',
    socketPath: '/var/run/docker.sock',
  }).then(requestPromise);
