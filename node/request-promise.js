const http = require('http');

module.exports = options =>
  new Promise((resolve, reject) =>
    http
      .request(options, response => {
        let data = '';
        let rejected = false;

        if (response.statusCode !== 200) {
          return reject(new Error(`Unexpected Status: ${response.statusCode}`));
        }

        response.setEncoding('utf8');

        response.on('data', chunk => (data += chunk));

        response.on('error', err => reject(err));

        response.on('end', () => rejected || resolve(data));
      })
      .on('error', err => reject(err))
      .end(),
  );
