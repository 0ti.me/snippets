#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pass() { echo "PASS"; return 0; }
fail() { echo "FAIL"; return -1; }

main() {
  pass || { echo "This will not print" && return -1; }

  fail || { echo "This will print" && return -1; }

  echo "This will not print"
}

main "$@"

echo "Return code: $?"
