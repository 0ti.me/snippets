#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pass() {
  echo PASS

  return 0
}

fail() {
  echo FAIL

  return -1
}

main() {
  local TEST=
  local FAIL=

  TEST="$(pass)" || return 0

  echo "${TEST}"

  local ODDLY="$(fail)" || return 0

  echo "This will oddly print ${ODDLY}"

  FAIL="$(fail)" || return 0

  echo "This won't print ${FAIL}"
}

main "$@"

echo "Return code: $?"
