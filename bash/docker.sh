#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

main() {
  curl \
    --unix-socket /var/run/docker.sock \
    http://localhost/containers/json
}

main "$@"
